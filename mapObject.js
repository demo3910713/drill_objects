function mapObject(cb, obj) {
  if (typeof obj == 'object') {
    let map = [];
    for (const key in obj) {
      const element = obj[key];
      cb(element, key, map);
    }
    return map
  } else {
    return "Data Insufficient";
  }
}
// callback function
function mapping(values, key, map) {
  map.push(`${key} : ${values}`);
}
module.exports = { mapObject, mapping };

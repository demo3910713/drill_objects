const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
   if (typeof obj == 'object') {
    let pairing = []
    for(const key in obj)
    {
      let subPair = [key +" : " +obj[key]]
      pairing.push(subPair)
    }
    return pairing
   } else {
    return "Data Insufficeint "
   }
}
module.exports = { pairs}

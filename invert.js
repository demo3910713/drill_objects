function invert(obj) {
  if (typeof obj == "object") {
    let invert = {};
    for (const key in obj) {
      const value = obj[key];
      invert[value] = key;
    }
    return invert;
  } else {
    return "Data Insufficeint ";
  }
}

module.exports = { invert };
